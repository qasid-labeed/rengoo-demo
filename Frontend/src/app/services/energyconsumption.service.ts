import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class EnergyConsumptionService {
  // ConsumptionEnergyMap:any = 'https://hyperledger-power-watson-rest-v12-fearless-genet.eu-gb.mybluemix.net/api/User/';
  ConsumptionEnergyMap: any = "http://158.175.133.232:3000/api/User/";
  constructor(private http: HttpClient) {}

  //5bcd8c59a1393d25f9c768cf
  getEnergyConsumption(Userid: any) {
    return this.http
      .get(this.ConsumptionEnergyMap + Userid, {
        headers: new HttpHeaders().append("Content-Type", "application/json"),
      })
      .pipe(map((res) => res));
  }
}
