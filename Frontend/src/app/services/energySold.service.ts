import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";

// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class EnergySoldService {
  SoldEnergyMap: any = "http://158.175.133.232:3000/api/User/";
  SoldEnergyMaps: any = "../../assets/img/boundary.json";
  urlSellEnergy = "http://158.175.133.232:3000/api/addEnergyProduction";
  constructor(private http: HttpClient) {}

  sellEnergy(data: any): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.urlSellEnergy, data, {
      headers: new HttpHeaders().append("Content-Type", "application/json"),
    });
  }

  getEnergySold(userid: any) {
    return this.http
      .get(this.SoldEnergyMap + userid, {
        headers: new HttpHeaders().append("Content-Type", "application/json"),
      })
      .pipe(map((res) => res));
  }

  getBermuda() {
    return this.http
      .get(this.SoldEnergyMaps, {
        headers: new HttpHeaders().append("Content-Type", "application/json"),
      })
      .pipe(map((res) => res));
  }
}
