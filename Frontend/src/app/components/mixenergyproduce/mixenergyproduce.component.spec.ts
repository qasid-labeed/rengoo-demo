import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixenergyproduceComponent } from './mixenergyproduce.component';

describe('MixenergyproduceComponent', () => {
  let component: MixenergyproduceComponent;
  let fixture: ComponentFixture<MixenergyproduceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixenergyproduceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixenergyproduceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
