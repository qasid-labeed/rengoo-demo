import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { AuthService } from '../../../services/auth.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserPanelNavbarComponent } from '../user-panel-navbar.component';


declare var $: any;

@Component({
  selector: "app-user-trade",
  templateUrl: "./user-trade.component.html",
  styleUrls: ["./user-trade.component.css"]
})
export class UserTradeComponent implements OnInit {

  //@ViewChild(UserPanelNavbarComponent) navbar: UserPanelNavbarComponent;

  energyProductionDetails: any;
  responseData = [];
  prosumer = [];
  userName = "Username";
  individualData: any;
  resouceEngergy: any;
  energyResourceById: any;
  totalAvailableById: any;
  userId: any;
  sellerId: any;
  energyById: any;
  userType: any;
  buyersopenToOffer = [];
  resourceEnergySeller = [];
  productionDetailSeller = [];
  openOffersUsers: any = [];
  buyForm: FormGroup;

  buyerName: String = "";
  offersFromSellers = [];

  dtOptions: DataTables.Settings = {};
  dtTriggers: Subject<any> = new Subject();

  approvalObject: any = {};
  approvalForm: FormGroup;

  constructor(
    private dataService: DataService,
    private authService: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.spinnerService.show();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 3,
      processing: true
    };
    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {

      this.userType = res["userType"];
      this.userName = res["name"];
      if (this.userType == 'PROSUMER') {
        this.prosumer = res['energyProductionDetails'];
      }

      for (let i = 0; i < res["openOffersList"].length; i++) {
        this.dataService.getMix(res["openOffersList"][i].seller.split('#')[1]).subscribe(response => {
          res["openOffersList"][i].sellerName = response["name"];
          res["openOffersList"][i].totalAvailable = response["totalAvailableProduction"];

        })
      }
      this.offersFromSellers = res["openOffersList"];
      this.spinnerService.hide();
    }, err => {
      this.spinnerService.hide();
    });

    this.buyForm = this.formBuilder.group({
      purchaseUnitKWH: ['', [
        Validators.required,
        Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')
      ]]
    })


    this.approvalForm = this.formBuilder.group({
      units: ['', [
        Validators.required,
        Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')
      ]]
    })

    this.powerStoarageData();

    this.buyerOpenToOfferData();

    this.userId = JSON.parse(localStorage.getItem("user")).id;

  }

  get units() {
    return this.approvalForm.get('units');
  }

  getUserName(param) {
    this.dataService.getMix(param).subscribe(res => {
      return res["name"];
    })
    // return param;
  }
  // Get All Producers and Prosumers for Trade Page
  powerStoarageData() {
    this.dataService.getPowerStorateData().subscribe(res => {
      this.responseData = [];

      this.arrayMap(res, item => {
        if (item.energyProductionDetails.length > 0 && item.userID != (JSON.parse(localStorage.getItem("user")).id)) {
          this.responseData.push(item);
        }
      });
      this.dtTriggers.next();

      this.spinnerService.hide();
    }, err => {
      this.spinnerService.hide();
    });
  }

  get purchaseUnitKWH() {
    return this.buyForm.get('purchaseUnitKWH');
  }

  buyerOpenToOfferData() {
    this.dataService.getBuyersOpenToOffer().subscribe(res => {
      this.arrayMap(res, item => {

        this.buyersopenToOffer.push(item);
        this.spinnerService.hide();

      });
    }, err => {
      this.spinnerService.hide();
    });

  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i]));
    }
    return aray;
  }

  setBuyer(param) {
    this.buyerName = param;
  }

  openEnergyModal(id: any, energy: any) {

    this.sellerId = id;
    this.energyById = energy;


    this.spinnerService.show();
    this.dataService.getPowerStorageDataByUserId(id).subscribe(res => {

      this.arrayMap(res["energyProductionDetails"], item => {
        if (item.energyResource == this.energyById) {

          this.energyResourceById = item.energyResource;
          this.totalAvailableById = item.totalAvailable;
        }
        // $("#sell-energy").modal("hide");
      })
      //}

      // this.resouceEngergy = this.individualData["energyProductionDetails"][0][
      //   "energyResource"
      // ];
      // this.productionDetail = this.individualData["totalAvailableProduction"];
      // this.resourceEnergySeller = this.individualData["energyProductionDetails"][0].energyResource;
      // this.productionDetailSeller = this.individualData["energyProductionDetails"][0].production

      // console.log(
      //   "Result",
      //   this.individualData["energyProductionDetails"][0]["energyResource"]
      // );
      // console.log(
      //   "Production",
      //   this.individualData["totalAvailableProduction"]
      // );

      this.spinnerService.hide();
    }, err => {
      this.spinnerService.hide();
    });
  }


  openEnergyOfferApprovalModal(resObj) {
    this.approvalObject = resObj;

  }

  submitApproval() {
    this.spinnerService.show();
    let obj = {
      $class: `waltson.poc.hyperledger.buyEnergy`,
      buyer: `resource:waltson.poc.hyperledger.User#${JSON.parse(localStorage.getItem("user")).id}`,
      seller: this.approvalObject.seller,
      grid: `resource:waltson.poc.hyperledger.Grid#${111}`,
      purchaseUnitKWH: parseFloat(this.units.value),
      energyResource: this.approvalObject.energyResource,
      approval: true

    };

    this.dataService.buyEnergy(obj).subscribe(res => {
      this.toastr.success(`Successfull purchased from buyer ${this.approvalObject.sellerName} `, "Success");
      $("#accept-energy-proposal").modal("hide");
      this.spinnerService.hide();
      this.ngOnInit();

      //Refrest Navbar Component to Refrest Balance
      //this.navbar.updateBalance();

      //To prevent Data Table Error
      $.fn.dataTable.ext.errMode = 'throw';
    }, err => {
      this.toastr.error(`Cannot approve.May you have out of balance or buyer has less production power.`, "Error");
      $("#accept-energy-proposal").modal("hide");
      this.spinnerService.hide();

    })
  }


  submit() {
    // console.log("Purcahse Number" + purchaseAmount)
    // console.log(f.value.purchase);
    // console.log("UserId is inside buy engery", this.userId);

    if (this.userId) {

      this.spinnerService.show();
      var sellerDetail = {
        $class: "waltson.poc.hyperledger.buyEnergy",
        buyer: "resource:waltson.poc.hyperledger.User#" + this.userId,
        seller: "resource:waltson.poc.hyperledger.User#" + this.sellerId,
        grid: "resource:waltson.poc.hyperledger.Grid#111",
        purchaseUnitKWH: parseInt(this.purchaseUnitKWH.value),
        energyResource: this.energyResourceById,
        approval: true
      };

      this.dataService.buyEnergy(sellerDetail).subscribe(res => {
        if (res) {
          this.toastr
            .success("Energy brought sucessfully", "Success"); //example
          $("#sell-energy").modal("hide");

          this.spinnerService.hide();
          this.ngOnInit();
        }

      }, err => {
        // console.log('Error ', err.error.error.message.split(':')[5]);
        this.toastr
          .error(`${err.error.error.message.split(':')[7]}`, JSON.parse(localStorage.getItem("user")).username);

        this.spinnerService.hide();


      });
    }
  }
}
