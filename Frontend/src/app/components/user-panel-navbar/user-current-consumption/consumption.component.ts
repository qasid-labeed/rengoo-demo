import { Component, OnInit ,OnDestroy} from "@angular/core";
import { DataService } from '../../../services/data.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
declare var Highcharts: any;
import { Subject } from 'rxjs';

@Component({
  selector: 'app-current-consumption',
  templateUrl: './consumption.component.html',
  styleUrls: ['./consumption.component.css']
})

export class CurrentConsumptionComponent implements OnInit {
  user: string;
  energies: any;
  timestamps: any;
  userBalanceAmount: any;
  totalEnergyConsumed: any;
  userId: any;
  // consumptions[]:any;
  EnergyPercentage: any;
  EnergyName: any;
  userName;
  totalConsumption: number = 0;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(

    private dataService: DataService,
    private spinnerService: Ng4LoadingSpinnerService,
  ) {


  }

  ngOnInit() {
    this.spinnerService.show();
    this.userName = JSON.parse(localStorage.getItem("user")).username;
    this.getComsumption();

    setInterval(() => { 
    this.getComsumption();
    }, 5000);
  }

  getComsumption() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
    this.dataService.getConsumption(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {
      var new_energies = this.arrayMap(res, item => item.energyConsumed).reverse();
      var new_timestamps = this.arrayMap(res, item => item.timestamp.split("T")[0]).reverse(); 
      if(JSON.stringify(this.energies) != JSON.stringify(new_energies) && 
         JSON.stringify(this.timestamps) != JSON.stringify(new_timestamps)){
          this.energies = new_energies;
          this.timestamps = new_timestamps;
          this.arrayMap(res, item => {
            this.totalConsumption += item.energyConsumed;
          });
           this.dtTrigger.next();
          this.renderHighchart();
          this.spinnerService.hide();
        }
    }, err => {
      this.spinnerService.hide();
    });

    
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i]));
    }
    return aray;
  }


  renderHighchart() {
    Highcharts.chart('consumption', {
      chart: {
        type: 'areaspline'
      },
      title: {
        text: 'Consumption Usage'
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      xAxis: {
        categories: this.timestamps,
        plotBands: [{ // visualize the weekend
          from: 4.5,
          to: 6.5,
          color: 'rgba(68, 170, 213, .2)'
        }]
      },
      yAxis: {
        title: {
          text: 'KW'
        }
      },
      tooltip: {
        shared: true,
        valueSuffix: ' KW'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.5
        }
      },
      series: [{
        name: 'Energy Consumed',
        data: this.energies
      }]
    });
  }

  
}