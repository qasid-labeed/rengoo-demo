import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {DataService} from '../../../services/data.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-user-power-storage',
  templateUrl: './user-power-storage.component.html',
  styleUrls: [ './user-power-storage.component.css']
})
export class UserPowerStorageComponent implements OnInit {
  
  batteryCapacity: any;
  totalEnergyConsumed: any;
  totalBought: any;
  totalProduction: any;

  total: any = 0;

  constructor(private authService: AuthService, private dataService: DataService, private spinnerService: Ng4LoadingSpinnerService ) { }

  ngOnInit() {
    this.spinnerService.show();
      this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {

        this.batteryCapacity = res['batteryCapacity'];
        this.totalEnergyConsumed = res['totalEnergyConsumed'];
        this.totalBought = res['totalBoughtEnergy'];
        this.totalProduction = res['totalProduction'];

        this.total = (this.totalProduction + this.totalBought) - this.totalEnergyConsumed;
        this.total > this.batteryCapacity ? this.total = 100 : this.total = (this.total / this.batteryCapacity) * 100;
        this.spinnerService.hide();
      }, err => {
        this.spinnerService.hide();
      });    
  }
}
