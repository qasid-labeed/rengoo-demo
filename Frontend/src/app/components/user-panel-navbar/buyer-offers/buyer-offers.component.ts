import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { AuthService } from '../../../services/auth.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-buyer-offers',
    templateUrl: './buyer-offers.component.html',
    styleUrls: [ './buyer-offers.component.css' ]
})

export class BuyerOffersComponent {

  energyProductionDetails: any;
  responseData = [];
  prosumer = [];
  userName = "Username";
  individualData: any;
  resouceEngergy: any;
  productionDetail: any;
  userId: any;
  sellerId: any;
  userType: any;
  buyersopenToOffer = [];
  resourceEnergySeller = [];
  productionDetailSeller = [];
  openOffersUsers: any = [];

  constructor(
    private dataService: DataService,
    private authService: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.spinnerService.show();
    this.dataService.getMix(JSON.parse(localStorage.getItem("user")).id).subscribe(res => {
      this.userType = res["userType"];
      this.userName = res["name"];
      if (this.userType == 'PROSUMER') {
        this.prosumer = res['energyProductionDetails'];
      }

      if (this.userType == 'PROSUMER' || this.userType == 'PRODUCER') {
        this.dataService.getAllOpenOffers().subscribe(res => {      

          for( var i = 0; i < res.body.length; i++){ 
            if ( res.body[i].name == this.userName) {
              res.body.splice(i, 1); 
            }
         }
          this.openOffersUsers = res.body;
          this.spinnerService.hide();
        }, err => {
          this.spinnerService.hide();
        });
      }

    }, err => {
      this.spinnerService.hide();
    });

    this.powerStoarageData();
    this.buyerOpenToOfferData();

    this.userId = JSON.parse(localStorage.getItem("user")).id;
  }

  powerStoarageData() {
    this.dataService.getPowerStorateData().subscribe(res => {
      this.responseData = [];
      this.arrayMap(res, item => {
        if (item.energyProductionDetails.length > 0) {
          this.responseData.push(item);
        }
      });
      this.spinnerService.hide();
    }, err => {
      this.spinnerService.hide();
    });
  }

  buyerOpenToOfferData() {
    this.dataService.getBuyersOpenToOffer().subscribe(res => {
      this.arrayMap(res, item => {
        this.buyersopenToOffer.push(item);
      });
    }, err => {
      this.spinnerService.hide();
    });
  }

  arrayMap(obj, fn) {
    var aray = [];
    for (var i = 0; i < obj.length; i++) {
      aray.push(fn(obj[i]));
    }
    return aray;
  }

  openEnergyModal(id: any) {
    this.sellerId = id;
    window.localStorage.setItem("buyer_id",this.sellerId);


    this.spinnerService.show();
    this.dataService.getPowerStorageDataByUserId(id).subscribe(res => {
      this.individualData = res;
      this.resouceEngergy = this.individualData["energyProductionDetails"][0][
        "energyResource"
      ];
      this.productionDetail = this.individualData["totalAvailableProduction"];
      this.resourceEnergySeller = this.individualData["energyProductionDetails"][0].energyResource;
      this.productionDetailSeller = this.individualData["energyProductionDetails"][0].production

      // console.log(
      //   "Result",
      //   this.individualData["energyProductionDetails"][0]["energyResource"]
      // );
      // console.log(
      //   "Production",
      //   this.individualData["totalAvailableProduction"]
      // );

      this.spinnerService.hide();
    }, err => {
      this.spinnerService.hide();
    });
  }

  submit(f) {
    var purchaseAmount = f.value.purchase;


    if (this.userId) {
      var sellerDetail = {
        $class: "waltson.poc.hyperledger.buyEnergy",
        buyer: "resource:waltson.poc.hyperledger.User#" + this.userId,
        seller: "resource:waltson.poc.hyperledger.User#" + this.sellerId,
        grid: "resource:waltson.poc.hyperledger.Grid#111",
        purchaseUnitKWH: parseInt(f.value.purchase),
        energyResource: this.resouceEngergy
      };
      this.spinnerService.show();

      this.dataService.buyEnergy(sellerDetail).subscribe(res => {
        
        if (res) {
          this.toastr
            .success("Energy brought sucessfully", "Success"); //example
        //   $("#sell-energy").modal("hide");
          this.spinnerService.hide();
        }
      }, err => {
        this.toastr
          .error("Seller production is less then your purchase.", "Error");

        this.spinnerService.hide();

      });
    }
  }
}